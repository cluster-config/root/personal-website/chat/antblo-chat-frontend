import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MessageWindowComponent } from './message-window/message-window.component';
import { MessageInputComponent } from './message-input/message-input.component';
import { MessageComponent } from './message-window/message/message.component';
import { UsernameInputComponent } from './username-input/username-input.component';

@NgModule({
  declarations: [
    AppComponent,
    MessageWindowComponent,
    MessageInputComponent,
    MessageComponent,
    UsernameInputComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
