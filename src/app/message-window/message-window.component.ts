import { Component, OnInit } from '@angular/core';
import { Message, MessageService } from '../message.service';


@Component({
  selector: 'app-message-window',
  templateUrl: './message-window.component.html',
  styleUrls: ['./message-window.component.scss']
})
export class MessageWindowComponent implements OnInit {
  messages: Array<Message>;
  
  constructor(messageService: MessageService) { 

    this.messages = messageService.messages;
  }

  ngOnInit(): void {
  }
}
