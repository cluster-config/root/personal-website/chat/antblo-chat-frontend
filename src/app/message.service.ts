import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import * as signalR from "@microsoft/signalr";
import { HubConnection } from '@microsoft/signalr';

export type Message = { username: string, text: string }

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: Array<Message> = [{ username: "Anton", text: "Hello" }];
  connection: HubConnection

  constructor() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl("/hub")
      .build();
  }

  async sendEmail(email: string) {
    this.connection.send("SendEmail", email);
  }
  async sendToken(token: string) {
    this.connection.send("SendToken", token);
  }

  async sendMessage(message: string) {
    this.connection.send("SendMessage", message);
  }

  async initService() {
    this.connection.on("id", (id: string) => {
      console.log(id)
    });

    await this.connection.start().catch(err => document.write(err));
    //this.connection.send("id", "antonblomstrom97@gmail.com")
    //this.connection.send("SendMessageToAnton", "text")
  }
}
