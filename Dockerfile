FROM node:14.14.0-buster AS base
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular


FROM base AS build
WORKDIR /app
COPY . .
RUN npm install
RUN ng build --prod


FROM node:14.14.0-buster-slim AS final
WORKDIR /app
COPY --from=build /app/dist /app/dist
RUN npm install -g serve
EXPOSE 5000
CMD serve /app/dist/antblo-chat
