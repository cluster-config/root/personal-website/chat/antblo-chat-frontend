import { Component, OnInit } from '@angular/core';
import { MessageService } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'chat';
  messageService: MessageService;
  
  constructor(messageService: MessageService){
    this.messageService = messageService
  }

  async ngOnInit() {
    await this.messageService.initService();
  }
}
